﻿using System;

namespace N1operat_overload
{
    public class Stretch
    {
        private double length;

        public void SetLength(double a)
        {
            this.length = a;
        }
        public double GetLength()
        {
            return length;
        }

        public static Stretch operator +(Stretch a, Stretch b){
            Stretch number = new Stretch();
            number.length = a.length + b.length;
            return number;
        }
        public static Stretch operator -(Stretch a, Stretch b){
            Stretch number = new Stretch();
            number.length = a.length - b.length;
            return number;
        }
        public static Stretch operator /(Stretch a, Stretch b){
            Stretch number = new Stretch();
            number.length = a.length / b.length;
            return number;
        }
        public static Stretch operator *(Stretch a, Stretch b){
            Stretch number = new Stretch();
            number.length = a.length * b.length;
            return number;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Stretch A = new Stretch();
            Stretch B = new Stretch();
            Stretch C = new Stretch();
            A.SetLength(12.2);
            B.SetLength(6.1);
            Console.WriteLine(A.GetLength());
            Console.WriteLine(B.GetLength());

            C = A + B;
            Console.WriteLine(C.GetLength());

            C = A - B;
            Console.WriteLine(C.GetLength());

            C = A / B;
            Console.WriteLine(C.GetLength());

            C = A * B;
            Console.WriteLine(C.GetLength());
            Console.WriteLine(C.GetLength());
            Console.WriteLine(C.GetLength());

        }
    }
}
